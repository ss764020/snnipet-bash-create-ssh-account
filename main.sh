#!/bin/bash
cd `dirname $0`
IFS=,

# 変数定義
FSTAB="_tmp_fstab.txt"
SSHD_CONFIG="_tmp_sshd_config.txt"

# 変数読み込み
source ./.conf_setting.conf

# 一時テキストファイルの初期化
cp /dev/null $FSTAB
cp /dev/null $SSHD_CONFIG

# conf_userで定義されたユーザごとの動作
echo ''
echo '========================================================='
echo '1. add user'
echo '========================================================='
cat .conf_user.conf | while read -a usr
do
  echo $usr | grep -v '^#.*' > /dev/null
  if [ $? -eq 0 ];then
    echo ''
    echo "【user - '${usr[1]}'】"

    # ユーザの作成
    if getent passwd ${usr[1]} > /dev/null 2>&1; then
      # すでに作成済み
      echo "- user '${usr[1]}' is already exist."
    elif [ ! -f ./keys/${usr[1]}.pub ]; then
      # keyディレクトリに公開鍵が存在しない
      echo "- './keys/${usr[1]}.pub' is not found."
    else
      sudo useradd ${usr[1]}
      if [ -n "${USER_MOD}" ]; then
        sudo usermod -aG ${USER_MOD} ${usr[1]}
      fi
      sudo mkdir /home/${usr[1]}/.ssh/
      echo "- user '${usr[1]}' is added."

      # SSHユーザの場合、wheelグループに追加
      if [ ${usr[0]} = "ssh" ];then
        sudo usermod -aG wheel ${usr[1]}
      fi
    fi

    # 公開鍵の設置（鍵の更新もあり得るので毎回上書く）
    if [ -f ./keys/${usr[1]}.pub ]; then
      sudo cp keys/${usr[1]}.pub /home/${usr[1]}/.ssh/authorized_keys
      sudo chown -R ${usr[1]}:${usr[1]} /home/${usr[1]}/.ssh/
      sudo chmod 700 /home/${usr[1]}/.ssh/
      sudo chmod 600 /home/${usr[1]}/.ssh/authorized_keys
      echo "- public keyfile '/home/${usr[1]}/.ssh/authorized_keys' is updated."
    else
      echo "- public keyfile './keys/${usr[1]}.pub' is not found."
    fi

    # SFTPユーザ作成時のみの動作
    if [ ${usr[0]} = "sftp" ];then
      # ホームディレクトリの作成
      if [ ! -d /home/${usr[2]}/ ]; then
        sudo mkdir /home/${usr[2]}/
        sudo chown root:root /home/${usr[2]}/
        sudo chmod 755 /home/${usr[2]}/

        # /etc/ssh/sshd_config の設定内容
        echo "## '${usr[1]}' のユーザ設定 (added by y.ohta)" >> $SSHD_CONFIG
        echo "match user ${usr[1]}" >> $SSHD_CONFIG
        echo "  ChrootDirectory /home/${usr[2]}/" >> $SSHD_CONFIG
        echo "  ForceCommand internal-sftp -u 0002" >> $SSHD_CONFIG
        echo "- created this sftpuser's userroot directory '/home/${usr[2]}/'."
      fi

      # ディレクトリマウント
      # /etc/fstab の設定内容
      echo "" >> $FSTAB
      echo "## ${usr[1]}" >> $FSTAB

      cat .conf_project.conf | while read prj
      do
        echo $prj | grep -v '^#.*' > /dev/null
        if [ $? -eq 0 ];then
          # プロジェクトディレクトリをマウント
          if [ ! -d /home/${usr[2]}/$prj/ ]; then
            sudo mkdir /home/${usr[2]}/$prj/
            sudo mount -B $DOC_ROOT_DIR$prj$PROJECT_DIR /home/${usr[2]}/$prj/

            # /etc/fstab の設定内容
            echo "${DOC_ROOT_DIR}${prj}${PROJECT_DIR}   /home/${usr[2]}/${prj}/            none    bind            0 0" >> $FSTAB
            echo "- mount directory '/home/${usr[2]}/$prj/'."
          else
            echo "- directory '/home/${usr[2]}/$prj/' is already mounted."
          fi
        fi
      done
    fi
  fi
done

# bash実行後の作業方法を指示するためのテキスト出力
echo ''
echo '========================================================='
echo '2. please check the following, And complete the setting.'
echo '========================================================='
echo ''
echo '(1) add following text to /etc/ssh/sshd_config'
echo '$ sudo vi /etc/ssh/sshd_config'
echo '-------'
cat $SSHD_CONFIG
echo '-------'
echo ''
echo '(2) check syntax error & reload sshd'
echo '$ sudo sshd -t'
echo '$ sudo service sshd reload'
echo ''
echo '(3) add following text to /etc/fstab'
echo '$ sudo vi /etc/fstab'
echo '-------'
cat $FSTAB
echo '-------'
echo ''
echo '========================================================='
echo '- fin. -'
echo '========================================================='
