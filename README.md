# 0.前提条件

### sshd_configにて下記のように設定済みであること

$ sudo vi /etc/ssh/sshd_config

##### パスワード認証を禁止

```
#PasswordAuthentication yes
PasswordAuthentication no
```

##### RSA認証を許可（ssh1のみ）

```
#RSAAuthentication no
RSAAuthentication yes
```

##### 公開鍵認証を許可（ssh2のみ）

```
#PubkeyAuthentication no
PubkeyAuthentication yes
```

##### rhost認証を禁止

```
#RhostsAuthentication yes
RhostsAuthentication no
```

# 1. 利用方法

#### (1) 下記コマンド実行

```bash
$ cd ~/
$ sudo yum install git
$ git clone https://bitbucket.org/ss764020/snnipet-bash-create-ssh-account.git
$ cd snnipet-bash-create-ssh-account/
$ cp .conf_project.conf.sample .conf_project.conf
$ cp .conf_setting.conf.sample  .conf_setting.conf
$ cp .conf_user.conf.sample .conf_user.conf
```

#### (2) confファイル編集

```bash
$ vi .conf_project.conf
$ vi .conf_setting.conf
$ vi .conf_user.conf
```

#### (3) ~/snnipet-bash-create-ssh-account/keys/ ディレクトリに各スタッフの公開鍵ファイルを設置  
（※ [GoogleDrive](https://drive.google.com/drive/folders/1GqIOYPwijmUMwjILMWHlvGclEut5R8eI)よりダウンロード可）  
  
#### (4) bashファイル実行

```bash
$ bash main.sh
```

#### (5) bashファイルを実行すると、下記の例のように操作の指示が出力されるので、これに従ってsshd_configとfstabの修正を完了する。  
  
例）

```
=========================================================
2. please check the following, And complete the setting.
=========================================================

(1) add following text to /etc/ssh/sshd_config
$ sudo vi /etc/ssh/sshd_config
-------
## 'sftpuser-n.nagahama' のユーザ設定 (added by y.ohta)
match user sftpuser-n.nagahama
  ChrootDirectory /home/html-n.nagahama/
  ForceCommand internal-sftp -u 0002
-------

(2) check syntax error & reload sshd
$ sudo sshd -t
$ sudo service sshd reload

(3) add following text to /etc/fstab
$ sudo vi /etc/fstab
-------

## sftpuser-n.nagahama
-------
```

#### (6) （現在開いているSSHコンソールを閉じずに）SSHの接続テストをしてみて、接続ができていれば完了